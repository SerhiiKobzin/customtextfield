//
//  CustomTextField.swift
//  Test
//
//  Created by Serhii Kobzin on 8/3/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

//@IBDesignable
class CustomTextField: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var borderViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewTopTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewBottomTopConstraint: NSLayoutConstraint!
    
    weak var delegate: UITextFieldDelegate?
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            borderView.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            borderView.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            borderView.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var placeholder: String = "" {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.foregroundColor: placeholderColor,
                                                                                                   .font: UIFont.systemFont(ofSize: placeholderFontSize, weight: .medium)])
            titleLabel.text = placeholder
            titleLabel.setNeedsLayout()
            titleLabel.layoutIfNeeded()
            borderViewTopConstraint.constant = titleLabel.bounds.size.height / 2
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor(red: 154 / 255, green: 164 / 255, blue: 168 / 255, alpha: 1.0) {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.foregroundColor: placeholderColor,
                                                                                                   .font: UIFont.systemFont(ofSize: placeholderFontSize, weight: .medium)])
        }
    }
    
    @IBInspectable var placeholderFontSize: CGFloat = 13.0 {
        didSet {
            textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.foregroundColor: placeholderColor,
                                                                                                   .font: UIFont.systemFont(ofSize: placeholderFontSize, weight: .medium)])
        }
    }
    
    @IBInspectable var titleColor: UIColor = UIColor(red: 154 / 255, green: 164 / 255, blue: 168 / 255, alpha: 1.0) {
        didSet {
            titleLabel.textColor = placeholderColor
        }
    }
    
    @IBInspectable var titleFontSize: CGFloat = 11.0 {
        didSet {
            titleLabel.font = UIFont.systemFont(ofSize: titleFontSize, weight: .medium)
            titleLabel.setNeedsLayout()
            titleLabel.layoutIfNeeded()
            borderViewTopConstraint.constant = titleLabel.bounds.size.height / 2
        }
    }
    
    @IBInspectable var textColor: UIColor = UIColor(red: 19 / 255, green: 19 / 255, blue: 20 / 255, alpha: 1.0) {
        didSet {
            textField.textColor = textColor
        }
    }
    
    @IBInspectable var textFontSize: CGFloat = 17.0 {
        didSet {
            textField.font = UIFont.systemFont(ofSize: textFontSize, weight: .medium)
        }
    }
    
    @IBInspectable var errorColor: UIColor = UIColor(red: 255 / 255, green: 84 / 255, blue: 104 / 255, alpha: 1.0)
    
    var error: String? {
        didSet {
            if let error = error, !error.isEmpty {
                titleLabel.text = error
                titleLabel.textColor = errorColor
                borderView.layer.borderColor = errorColor.cgColor
            } else {
                titleLabel.text = placeholder
                titleLabel.textColor = placeholderColor
                borderView.layer.borderColor = borderColor.cgColor
            }
            titleLabel.setNeedsLayout()
            titleLabel.layoutIfNeeded()
            borderViewTopConstraint.constant = titleLabel.bounds.size.height / 2
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
}

extension CustomTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return false
        }
        if string.isEmpty && text.count == 1 {
            titleDown()
        } else if !string.isEmpty && text.isEmpty {
            titleUp()
        }
        if let shouldChange = delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) {
            return shouldChange
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let shouldBegin = delegate?.textFieldShouldBeginEditing?(textField) {
            return shouldBegin
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let shouldEnd = delegate?.textFieldShouldEndEditing?(textField) {
            return shouldEnd
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let shouldClear = delegate?.textFieldShouldClear?(textField) {
            return shouldClear
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let shouldReturn = delegate?.textFieldShouldReturn?(textField) {
            return shouldReturn
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.textFieldDidEndEditing?(textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        delegate?.textFieldDidEndEditing?(textField, reason: reason)
    }
    
}

extension CustomTextField {
    
    func commonInit() {
        Bundle.main.loadNibNamed("CustomTextField", owner: self, options: nil)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(contentView)
        textField.delegate = self
    }
    
    func titleDown() {
        titleViewTopTopConstraint.priority = UILayoutPriority(rawValue: 800)
        titleViewBottomTopConstraint.priority = UILayoutPriority(rawValue: 900)
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.titleView.alpha = 0.0
            self?.contentView.layoutIfNeeded()
        })
    }
    
    func titleUp() {
        titleViewTopTopConstraint.priority = UILayoutPriority(rawValue: 900)
        titleViewBottomTopConstraint.priority = UILayoutPriority(rawValue: 800)
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.titleView.alpha = 1.0
            self?.contentView.layoutIfNeeded()
        })
    }
    
}
