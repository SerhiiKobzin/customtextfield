//
//  ViewController.swift
//  Test
//
//  Created by Serhii Kobzin on 8/1/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var customTextField: CustomTextField!
    
    @IBAction func clicked(_ sender: UIButton) {
        customTextField.error = "USE MORE THAN 6 PASSWORD SYMBOLS"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customTextField.delegate = self
    }
    
}

extension ViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        customTextField.error = nil
        return true
    }
    
}
